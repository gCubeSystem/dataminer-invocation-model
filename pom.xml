<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">

	<modelVersion>4.0.0</modelVersion>
	<parent>
		<artifactId>maven-parent</artifactId>
		<groupId>org.gcube.tools</groupId>
		<version>1.1.0</version>
		<relativePath />
	</parent>

	<groupId>org.gcube.data.analysis</groupId>
	<artifactId>dataminer-invocation-model</artifactId>
	<version>1.0.0</version>
	<name>dataminer-invocation-model</name>
	<description>This library manages the dataminer-invocation model that is the xml format interpretable by DataMiner to invoke an algorithm execution</description>
	<scm>
		<connection>scm:git:https://code-repo.d4science.org/gCubeSystem/${project.artifactId}.git</connection>
		<developerConnection>scm:git:https://code-repo.d4science.org/gCubeSystem/${project.artifactId}.git</developerConnection>
		<url>https://code-repo.d4science.org/gCubeSystem/${project.artifactId}</url>
	</scm>


	<developers>
		<developer>
			<name>Francesco Mangiacrapa</name>
			<email>francesco.mangiacrapa@isti.cnr.it</email>
			<organization>CNR Pisa, Istituto di Scienza e Tecnologie dell'Informazione "A. Faedo"</organization>
			<roles>
				<role>architect</role>
				<role>developer</role>
			</roles>
		</developer>
	</developers>


	<dependencyManagement>
		<dependencies>
			<dependency>
				<groupId>org.gcube.distribution</groupId>
				<artifactId>maven-portal-bom</artifactId>
				<version>3.6.0</version>
				<type>pom</type>
				<scope>import</scope>
			</dependency>
			<!-- <dependency> -->
			<!-- <groupId>org.gcube.distribution</groupId> -->
			<!-- <artifactId>gcube-bom</artifactId> -->
			<!-- <version>LATEST</version> -->
			<!-- <type>pom</type> -->
			<!-- <scope>import</scope> -->
			<!-- </dependency> -->
		</dependencies>
	</dependencyManagement>


	<properties>
		<distroDirectory>${project.basedir}/distro</distroDirectory>
		<webappDirectory>${project.build.directory}/${project.build.finalName}</webappDirectory>
		<distroDirectory>distro</distroDirectory>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
		<project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>
		<!-- GWT needs at least java 1.6 -->
		<maven.compiler.source>1.7</maven.compiler.source>
		<maven.compiler.target>1.8</maven.compiler.target>
	</properties>

	<dependencies>

		<dependency>
			<groupId>com.sun.xml.bind</groupId>
			<artifactId>jaxb-core</artifactId>
			<version>2.3.0</version>
			<scope>provided</scope>
		</dependency>

		<dependency>
			<groupId>com.sun.xml.bind</groupId>
			<artifactId>jaxb-impl</artifactId>
			<version>2.3.0</version>
			<scope>provided</scope>
		</dependency>

		<dependency>
			<groupId>com.google.code.gson</groupId>
			<artifactId>gson</artifactId>
			<version>2.8.5</version>
			<scope>compile</scope>
		</dependency>

		<dependency>
			<groupId>org.eclipse.persistence</groupId>
			<artifactId>org.eclipse.persistence.moxy</artifactId>
			<version>2.6.6</version>
		</dependency>

		<dependency>
			<groupId>com.fasterxml.jackson.core</groupId>
			<artifactId>jackson-annotations</artifactId>
			<version>2.8.4</version>
		</dependency>

		<dependency>
			<groupId>org.projectlombok</groupId>
			<artifactId>lombok</artifactId>
			<version>1.14.8</version>
		</dependency>

		<!-- LOGGER -->
		<dependency>
			<groupId>log4j</groupId>
			<artifactId>log4j</artifactId>
			<scope>compile</scope>
		</dependency>
		<dependency>
			<groupId>org.slf4j</groupId>
			<artifactId>slf4j-log4j12</artifactId>
			<scope>compile</scope>
		</dependency>
		<dependency>
			<groupId>org.slf4j</groupId>
			<artifactId>slf4j-api</artifactId>
			<scope>compile</scope>
		</dependency>

		<!-- JUNIT -->
		<dependency>
			<groupId>junit</groupId>
			<artifactId>junit</artifactId>
			<version>4.8.1</version>
			<scope>test</scope>
		</dependency>

		<dependency>
			<groupId>commons-io</groupId>
			<artifactId>commons-io</artifactId>
			<version>2.6</version>
			<scope>test</scope>
		</dependency>

	</dependencies>

	<build>
		<!-- Generate compiled stuff in the folder used for developing mode -->
		<outputDirectory>${webappDirectory}/WEB-INF/classes</outputDirectory>
		<resources>
			<resource>
				<directory>src/main/java</directory>
				<includes>
					<include>**/*.*</include>
				</includes>
			</resource>
		</resources>

		<plugins>
			<plugin>
				<artifactId>maven-compiler-plugin</artifactId>
				<configuration>
					<source>${maven.compiler.source}</source>
					<target>${maven.compiler.target}</target>
				</configuration>
			</plugin>

<!-- 			<plugin> -->
<!-- 				<groupId>org.apache.maven.plugins</groupId> -->
<!-- 				<artifactId>maven-jar-plugin</artifactId> -->
<!-- 				<executions> -->
<!-- 					<execution> -->
<!-- 						<goals> -->
<!-- 							<goal>test-jar</goal> -->
<!-- 						</goals> -->
<!-- 					</execution> -->
<!-- 				</executions> -->
<!-- 			</plugin> -->

			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-surefire-plugin</artifactId>
				<configuration>
					<skipTests>true</skipTests>
				</configuration>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-resources-plugin</artifactId>
				<version>2.5</version>
				<executions>
					<execution>
						<id>copy-profile</id>
						<phase>install</phase>
						<goals>
							<goal>copy-resources</goal>
						</goals>
						<configuration>
							<outputDirectory>target</outputDirectory>
							<resources>
								<resource>
									<directory>${distroDirectory}</directory>
									<filtering>true</filtering>
									<includes>
										<include>profile.xml</include>
									</includes>
								</resource>
							</resources>
						</configuration>
					</execution>
				</executions>
			</plugin>

			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-assembly-plugin</artifactId>
				<configuration>
					<descriptors>
						<descriptor>descriptor.xml</descriptor>
					</descriptors>
				</configuration>
				<executions>
					<execution>
						<id>servicearchive</id>
						<phase>install</phase>
						<goals>
							<goal>single</goal>
						</goals>
					</execution>
				</executions>
			</plugin>
		</plugins>
	</build>
</project>
