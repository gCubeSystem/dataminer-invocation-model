
# Changelog

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## [v1.0.0] - 2021-01-13

[Task #20426] Migrate the "dataminer-invocation-model" component to git/jenkins and fix the jackson version


## [v0.2.0] - 2019-06-19

[Feature #16263] Added 'SHOW' value to ActionType enum


## [v0.1.0] - 2018-12-20

[Task #12975] First version
